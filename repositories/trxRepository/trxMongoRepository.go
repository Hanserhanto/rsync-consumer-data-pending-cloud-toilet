package trxRepository

import (
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
	"rsyncconsumerdatapendingtoilet/constans"
	"rsyncconsumerdatapendingtoilet/models"
	"rsyncconsumerdatapendingtoilet/repositories"
)

type trxMongoRepository struct {
	RepoDB repositories.Repository
}

func NewTrxMongoRepository(repoDB repositories.Repository) trxMongoRepository {
	return trxMongoRepository{
		RepoDB: repoDB,
	}
}

func (ctx trxMongoRepository) GetTrxListForSyncDataFailed() (trxList []models.Trx, err error) {
	limit := int64(10)

	filter := bson.M{
		"flagSyncData": false,
	}

	opt := &options.FindOptions{
		Sort:  bson.M{"docDate": 1},
		Limit: &limit,
	}

	collect := ctx.RepoDB.MongoDB.Collection(constans.TRX_COLLECTION)
	data, err := collect.Find(ctx.RepoDB.Context, filter, opt)
	if err != nil {
		return nil, err
	}

	for data.Next(ctx.RepoDB.Context) {
		var val models.Trx
		if err = data.Decode(&val); err != nil {
			return nil, err
		}

		trxList = append(trxList, val)
	}
	_ = data.Close(ctx.RepoDB.Context)

	return trxList, nil
}

func (ctx trxMongoRepository) UpdateTrxByInterface(filter interface{}, updateSet interface{}) error {

	upsert := true
	opt := &options.UpdateOptions{
		Upsert: &upsert,
	}

	collect := ctx.RepoDB.MongoDB.Collection(constans.TRX_COLLECTION)
	_, err := collect.UpdateOne(ctx.RepoDB.Context, filter, updateSet, opt)
	if err != nil {
		return err
	}

	return nil
}

func (ctx trxMongoRepository) RemoveTrxOutstanding(docNo string) error {

	filter := bson.M{
		"docNo": docNo,
	}

	opt := &options.DeleteOptions{}

	collect := ctx.RepoDB.MongoDB.Collection(constans.TRX_OUTSTANDING_COLLECTION)
	_, err := collect.DeleteOne(ctx.RepoDB.Context, filter, opt)
	if err != nil {
		return err
	}

	return nil
}

func (ctx trxMongoRepository) UpdateTrxOutstandingForClearSession(filter interface{}, updateSet interface{}) error {
	upsert := true
	opt := &options.UpdateOptions{
		Upsert: &upsert,
	}

	collect := ctx.RepoDB.MongoDB.Collection(constans.TRX_OUTSTANDING_FOR_CLEAR_SESSION_COLLECTION)
	_, err := collect.UpdateOne(ctx.RepoDB.Context, filter, updateSet, opt)
	if err != nil {
		return err
	}

	return nil
}

func (ctx trxMongoRepository) GetTrxOutstandingListForClearSession() (trx []models.TrxOutstandingForClearSession, err error) {
	var trxList []models.TrxOutstandingForClearSession

	filter := bson.M{
		"flagClearSession": false,
	}

	limit := int64(2)

	opt := &options.FindOptions{
		Limit: &limit,
	}

	collect := ctx.RepoDB.MongoDB.Collection(constans.TRX_OUTSTANDING_FOR_CLEAR_SESSION_COLLECTION)
	result, err := collect.Find(ctx.RepoDB.Context, filter, opt)
	if err != nil {
		return nil, err
	}

	for result.Next(ctx.RepoDB.Context) {
		var val models.TrxOutstandingForClearSession
		if err := result.Decode(&val); err != nil {
			return nil, err
		}

		trxList = append(trxList, val)
	}
	result.Close(ctx.RepoDB.Context)

	return trxList, nil
}

func (ctx trxMongoRepository) GetListTrxPending(dateFrom, dateTo string, limit int64) (*[]models.TrxItemsMongo, error) {
	var result []models.TrxItemsMongo
	//var limit int64 = 1000
	filter := make(map[string]interface{})
	filter["docDate"] = bson.M{"$gte": dateFrom, "$lte": dateTo}
	//filter["statusTrx"] = "03"
	//filter["docNo"] = "MPTO-23111321001424"

	option := options.FindOptions{
		Limit: &limit,
	}

	collect := ctx.RepoDB.MongoDB.Collection(constans.TRX_ITEMS_PREPAID_COLLECTION)
	data, err := collect.Find(ctx.RepoDB.Context, filter, &option)

	if err != nil {
		return nil, err
	}
	defer data.Close(ctx.RepoDB.Context)

	for data.Next(ctx.RepoDB.Context) {
		var logtrx models.TrxItemsMongo
		data.Decode(&logtrx)
		result = append(result, logtrx)
	}

	return &result, nil
}
