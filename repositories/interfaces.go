package repositories

import "rsyncconsumerdatapendingtoilet/models"

type TrxMongoRepository interface {
	GetTrxListForSyncDataFailed() (trxList []models.Trx, err error)
	UpdateTrxByInterface(filter interface{}, updateSet interface{}) error
	GetTrxOutstandingListForClearSession() (trx []models.TrxOutstandingForClearSession, err error)
	UpdateTrxOutstandingForClearSession(filter interface{}, updateSet interface{}) error
	RemoveTrxOutstanding(docNo string) error
	GetListTrxPending(dateFrom, dateTo string, limit int64) (*[]models.TrxItemsMongo, error)
}
