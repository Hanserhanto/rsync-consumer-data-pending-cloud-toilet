package services

import (
	"github.com/go-redis/redis"
	"github.com/streadway/amqp"
	"rsyncconsumerdatapendingtoilet/repositories"
)

type UsecaseService struct {
	RedisClient  *redis.Client
	TrxMongoRepo repositories.TrxMongoRepository
	RabbitMQ     *amqp.Channel
}

func NewUsecaseService(
	Redis *redis.Client,
	TrxMongoRepo repositories.TrxMongoRepository,
	RabbitMQ *amqp.Channel,
) UsecaseService {
	return UsecaseService{
		RedisClient:  Redis,
		TrxMongoRepo: TrxMongoRepo,
		RabbitMQ:     RabbitMQ,
	}
}
