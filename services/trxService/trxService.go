package trxService

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"rsyncconsumerdatapendingtoilet/config"
	"rsyncconsumerdatapendingtoilet/constans"
	"rsyncconsumerdatapendingtoilet/helpers"
	"rsyncconsumerdatapendingtoilet/models"
	"rsyncconsumerdatapendingtoilet/services"
	"rsyncconsumerdatapendingtoilet/utils"
	"strconv"
	"time"
)

type TrxService struct {
	Service services.UsecaseService
}

func NewTrxService(service services.UsecaseService) TrxService {
	return TrxService{
		Service: service,
	}
}

func (svc TrxService) SyncDataFailed() error {
	log.Println("[START] SyncDataPendingCloudToilet...")

	timeNow := time.Now()
	backDate := timeNow.AddDate(0, 0, -1)
	limit, _ := strconv.Atoi(config.LIMITTRXPENDING)

	dateTo := fmt.Sprintf("%s %s", timeNow.Format("2006-01-02"), "23:59:59")
	dateFrom := fmt.Sprintf("%s %s", backDate.Format("2006-01-02"), "00:00:00")

	resultData, err := svc.Service.TrxMongoRepo.GetListTrxPending(dateFrom, dateTo, int64(limit))
	if err != nil {
		fmt.Println("Error Get List Trx")
	}

	log.Println("resultData :", utils.ToString(resultData))
	for _, dataItems := range *resultData {
		var bulkItemsDetailInquiry []models.RequestParkingApps2pay
		requestApps2Pay := models.RequestApps2PayCheckStatus{
			MerchantKey:   dataItems.MerchantKey,
			MerchantNoRef: dataItems.DocNo,
			WithBatchCode: true,
		}

		requestByte, _ := json.Marshal(requestApps2Pay)
		responseCheckStatus, err := helpers.CallPaymentAPICheckStatus("POST", requestByte, fmt.Sprintf("%s", "http://8.215.46.83:15031/public/reporting/checkstatus"))
		if err != nil {
			fmt.Println("Error Get List Trx")
		}

		//err = json.Unmarshal(result, &responseCheckStatus)
		//if err != nil {
		//	log.Println("Err Unmarshall :", err.Error())
		//}

		if responseCheckStatus.Success == constans.TRUE && responseCheckStatus.Result.StatusPayment == constans.STATUS_SUCCESS_DESC {
			datailInquiryBatch := models.RequestParkingApps2pay{
				StatusCode:          responseCheckStatus.Result.StatusCode,
				Description:         constans.EMPTY_VALUE,
				MerchantNoRef:       dataItems.DocNo,
				TransactionLog:      dataItems.LogTrans,
				DeviceID:            responseCheckStatus.Result.DeviceID,
				NoHeader:            responseCheckStatus.Result.NoHeader,
				BankNoRef:           responseCheckStatus.Result.BankNoRef,
				CardType:            responseCheckStatus.Result.CardType,
				CardPan:             responseCheckStatus.Result.CardPan,
				MID:                 responseCheckStatus.Result.Mid,
				TID:                 responseCheckStatus.Result.Tid,
				LastBalance:         float64(responseCheckStatus.Result.LastBalance),
				CurrentBalance:      float64(responseCheckStatus.Result.CurrentBalance),
				PaymentCategoryName: responseCheckStatus.Result.PaymentCategory,
				PaymentStatus:       responseCheckStatus.Result.StatusPayment,
				TotalAmount:         float64(responseCheckStatus.Result.Amount),
				MDR:                 float64(responseCheckStatus.Result.Mdr),
				Discount:            float64(responseCheckStatus.Result.Discount),
				PromoCode:           responseCheckStatus.Result.PromoCode,
				PromoIssuer:         responseCheckStatus.Result.PromoIssuer,
				CreatedAt:           responseCheckStatus.Result.CreatedAt,
				SettleAt:            responseCheckStatus.Result.SettleAt,
				DeductAt:            responseCheckStatus.Result.SettleAt,
			}

			bulkItemsDetailInquiry = append(bulkItemsDetailInquiry, datailInquiryBatch)
			requestInqiryBatch := models.RequestInquiryBatchApp2Pay{
				BatchCode: responseCheckStatus.Result.BatchCode,
				Detail:    bulkItemsDetailInquiry,
			}

			fmt.Println(utils.ToString(requestInqiryBatch))
			bodyMsg, _ := json.Marshal(requestInqiryBatch)

			msg := amqp.Publishing{
				ContentType: "text/plan",
				Body:        bodyMsg,
			}

			fmt.Println("MASUK RABBIT")
			err = svc.Service.RabbitMQ.Publish("", "MPTO_TRX_PREPAID_CALLBACK", false, false, msg)
			if err != nil {
				log.Println("Error SyncTrxParking: ", err.Error())
			}
		} else {
			//var inquiryItems []models.InquiryItemsPrepaidBatchApp2Pay
			//var totalTrans float64
			//
			//dateNow := time.Now().Format("20060102")
			//prefix := fmt.Sprintf("%s-%s-", dataItems.OuCode, dateNow)
			//
			//batchCode, err := service.GenAutoNumRepo.AutonumberValueWithDatatype(constans.DATATYPE_TRX_ITEM_BATCH, prefix, 4)
			//if err != nil {
			//	fmt.Println("Error AutonumberValueWithDatatype : ", err)
			//}
			//
			//header := models.InquiryPrepaidBatchApp2Pay{
			//	BatchCode:   batchCode,
			//	MerchantKey: dataItems.MerchantKey,
			//}
			//
			//if dataItems.GrandTotal > 0 {
			//	items := models.InquiryItemsPrepaidBatchApp2Pay{
			//		MerchantKey:    dataItems.MerchantKey,
			//		MerchantNoRef:  dataItems.DocNo,
			//		Amount:         dataItems.GrandTotal,
			//		ServiceFee:     dataItems.ServiceFee,
			//		TransactionLog: dataItems.LogTrans,
			//		DeviceID:       *dataItems.DeviceID,
			//	}
			//
			//	totalTrans += items.Amount
			//	inquiryItems = append(inquiryItems, items)
			//}
			//
			//header.TotalData = int64(len(inquiryItems))
			//header.InquiryData = inquiryItems
			//header.TotalTransaction = totalTrans
			//
			//fmt.Println("Request Settlement :", utils.ToString(header))
			//if len(inquiryItems) > 0 {
			//	var err error
			//
			//	//suffixUrl, exists := svc.Service.WhitelistRepo.IsWhitelistRouteByCode(batch[0].OuCode)
			//	//if exists {
			//	//	_, err = helpers.WorkerExtCloud(header, fmt.Sprintf("%s%s", config.URL_APPS2PAY_DEV, suffixUrl))
			//	//} else {
			//	//}
			//	_, err = helpers.WorkerExtCloud(header, fmt.Sprintf("%s", "http://8.215.46.83:15031/public/bulkprepaid/settlement"))
			//	if err != nil {
			//		fmt.Println("Result Request WorkerInquiryPrepaidAPP", err.Error())
			//	}
			//}
			//fmt.Println("Done")

		}
	}

	log.Println("[END] SyncDataPendingCloudToilet...")
	return nil
}
