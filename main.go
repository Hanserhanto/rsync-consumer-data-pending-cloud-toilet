package main

import (
	"fmt"
	"github.com/streadway/amqp"
	"golang.org/x/net/context"
	"log"
	"rsyncconsumerdatapendingtoilet/app"
	"rsyncconsumerdatapendingtoilet/config"
	"rsyncconsumerdatapendingtoilet/repositories"
	"rsyncconsumerdatapendingtoilet/services/trxService"
	"time"
)

var (
	ctx = context.Background()
)

func main() {
	// Mongo DB connection using database default
	mongoDB := config.ConnectMongo(ctx)
	defer config.CloseMongo(ctx)

	// Connection for redis
	rdsConnection := config.ConnectRedis()

	connectionRabbitMQ, err := amqp.Dial(config.AMQPServerUrl)
	if err != nil {
		panic(err.Error())
	}
	defer connectionRabbitMQ.Close()

	channelRabbitMQ, err := connectionRabbitMQ.Channel()
	if err != nil {
		panic(err.Error())
	}
	defer channelRabbitMQ.Close()
	fmt.Println("Successful connected to RabbitMQ")

	// Configuration Repository
	repo := repositories.NewRepository(nil, mongoDB, ctx, nil)

	// Configuration Repository and Services
	svc := app.SetupApp(repo, rdsConnection, channelRabbitMQ)
	trxSvc := trxService.NewTrxService(svc)

	go termDuration(10, trxSvc)

	select {}
}

func termDuration(n time.Duration, trxSvc trxService.TrxService) {
	for _ = range time.Tick(n * time.Minute) {
		str := "Term Duration at " + time.Now().String()
		log.Println(str)

		err := trxSvc.SyncDataFailed()
		if err != nil {
			log.Println("Error Sync Data Failed:", err.Error())
		}

	}
}
