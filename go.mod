module rsyncconsumerdatapendingtoilet

go 1.16

require (
	github.com/aws/aws-sdk-go v1.54.8
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/joho/godotenv v1.5.1
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.4.2 // indirect
	github.com/lib/pq v1.10.9
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.33.1 // indirect
	github.com/streadway/amqp v1.1.0
	github.com/stretchr/testify v1.9.0 // indirect
	go.mongodb.org/mongo-driver v1.15.1
	golang.org/x/net v0.26.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
