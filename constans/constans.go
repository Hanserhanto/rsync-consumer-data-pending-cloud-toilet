package constans

const (
	DEFAULT_CHANNEL_REDIS                        = "MPTO_TRX_SYNC"
	CHANNEL_REDIS_PG_PARKING_CHECKOUT_EXCLUDE_SF = "PG_PARKING_CHECKOUT_EXCLUDE_SF"
	TRX_COLLECTION                               = "Trx"
	TRX_OUTSTANDING_COLLECTION                   = "TrxOutstanding"
	RSYNC_CLEAR_SESSION                          = "CLEAR_SESSION"
	TRX_OUTSTANDING_FOR_CLEAR_SESSION_COLLECTION = "TrxOutstandingForClearSession"
	TRX_ITEMS_PREPAID_COLLECTION                 = "trx_items_prepaid"
	TRUE                                         = true
	FALSE                                        = false
	STATUS_SUCCESS_DESC                          = "SUCCESS"
	STATUS_PENDING_DESC                          = "PENDING"
	STATUS_FAILED_DESC                           = "FAILED"
	EMPTY_VALUE                                  = ""
)
