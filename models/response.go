package models

import "time"

type Response struct {
	StatusCode       string      `json:"statusCode"`
	Success          bool        `json:"success"`
	ResponseDatetime time.Time   `json:"responseDatetime"`
	Result           interface{} `json:"result"`
	Message          string      `json:"message"`
}

type ResponseParkingGateway struct {
	StatusCode       string    `json:"statusCode"`
	Success          bool      `json:"success"`
	ResponseDatetime time.Time `json:"responseDatetime"`
	Result           struct {
		BatchNo    string `json:"batchNo"`
		Status     string `json:"status"`
		StatusDesc string `json:"statusDesc"`
	} `json:"result"`
	Message string `json:"message"`
}

type ResponseApps2PayCheckStatus struct {
	Message          string    `json:"message"`
	ResponseDatetime time.Time `json:"responseDatetime"`
	Result           struct {
		AcquiringID       int         `json:"acquiringID"`
		Amount            int         `json:"amount"`
		BankNoRef         string      `json:"bankNoRef"`
		BatchCode         string      `json:"batchCode"`
		CardPan           string      `json:"cardPan"`
		CardType          string      `json:"cardType"`
		CorporateName     string      `json:"corporateName"`
		CreatedAt         string      `json:"createdAt"`
		CurrentBalance    int         `json:"currentBalance"`
		DeviceID          string      `json:"deviceID"`
		Discount          int         `json:"discount"`
		InquiryWorkerResp interface{} `json:"inquiryWorkerResp"`
		LastBalance       int         `json:"lastBalance"`
		Mdr               float64     `json:"mdr"`
		Mid               string      `json:"mid"`
		NoHeader          string      `json:"noHeader"`
		PaymentCategory   string      `json:"paymentCategory"`
		PaymentFee        int         `json:"paymentFee"`
		PromoCode         string      `json:"promoCode"`
		PromoIssuer       string      `json:"promoIssuer"`
		ServiceFee        int         `json:"serviceFee"`
		SettleAt          string      `json:"settleAt"`
		StatusCode        string      `json:"statusCode"`
		StatusPayment     string      `json:"statusPayment"`
		Tid               string      `json:"tid"`
		VendorFee         int         `json:"vendorFee"`
	} `json:"result"`
	Status  string `json:"status"`
	Success bool   `json:"success"`
}
