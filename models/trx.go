package models

type Trx struct {
	DocNo            string                   `json:"docNo" bson:"docNo"`
	DocDate          string                   `json:"docDate" bson:"docDate"`
	DeviceId         string                   `json:"deviceId" bson:"deviceId"`
	Gate             string                   `json:"gate" bson:"gate"`
	IpGate           string                   `json:"ipGate" bson:"ipGate"`
	CardNumberUUID   string                   `json:"cardNumberUuid" bson:"cardNumberUuid"`
	CardNumber       string                   `json:"cardNumber" bson:"cardNumber"`
	TypeCard         string                   `json:"typeCard" bson:"typeCard"`
	ExtLocalDatetime string                   `json:"extLocalDatetime" bson:"extLocalDatetime"`
	GrandTotal       float64                  `json:"grandTotal" bson:"grandTotal"`
	ServiceFee       float64                  `json:"serviceFee" bson:"serviceFee"`
	ProductId        int64                    `json:"productId" bson:"productId"`
	ProductCode      string                   `json:"productCode" bson:"productCode"`
	ProductName      string                   `json:"productName" bson:"productName"`
	PaymentMethod    string                   `json:"paymentMethod" bson:"paymentMethod"`
	Price            float64                  `json:"price" bson:"price"`
	ProductData      PolicyOuProductWithRules `json:"productData" bson:"productData"`
	MemberName       string                   `json:"memberName" bson:"memberName"`
	MemberCode       string                   `json:"memberCode" bson:"memberCode"`
	DataMember       *MemberLocal             `json:"dataMember" bson:"dataMember"`
	OuId             int64                    `json:"ouId" bson:"ouId"`
	OuName           string                   `json:"ouName" bson:"ouName"`
	OuCode           string                   `json:"ouCode" bson:"ouCode"`
	MDR              float64                  `json:"mdr" bson:"mdr"`
	MID              string                   `json:"mid" bson:"mid"`
	TID              string                   `json:"tid" bson:"tid"`
	CardType         string                   `json:"cardType" bson:"cardType"`
	CardPan          string                   `json:"cardPan" bson:"cardPan"`
	LastBalance      float64                  `json:"lastBalance" bson:"lastBalance"`
	ResponseWorker   string                   `json:"responseWorker" bson:"responseWorker"`
	CurrentBalance   float64                  `json:"currentBalance" bson:"currentBalance"`
	LogTrans         string                   `json:"logTrans" bson:"logTrans"`
	MerchantKey      string                   `json:"merchantKey" bson:"merchantKey"`
	SignatureKey     string                   `json:"signatureKey" bson:"signatureKey"`
	StatusTransaksi  string                   `json:"statusTransaksi" bson:"statusTransaksi"`
	QrText           string                   `json:"qrText" bson:"qrText"`
	FlagSyncData     bool                     `json:"flagSyncData" bson:"flagSyncData"`
}

type TrxInvoiceItem struct {
	DocNo                  string  `json:"docNo" bson:"docNo"`
	ProductId              int64   `json:"productId" bson:"productId"`
	ProductCode            string  `json:"productCode" bson:"productCode"`
	ProductName            string  `json:"productName" bson:"productName"`
	IsPctServiceFee        string  `json:"isPctServiceFee" bson:"isPctServiceFee"`
	ServiceFee             float64 `json:"serviceFee" bson:"serviceFee"`
	ServiceFeeMember       float64 `json:"serviceFeeMember" bson:"serviceFeeMember"`
	Price                  float64 `json:"price" bson:"price"`
	BaseTime               int64   `json:"baseTime" bson:"baseTime"`
	ProgressiveTime        int64   `json:"progressiveTime" bson:"progressiveTime"`
	ProgressivePrice       float64 `json:"progressivePrice" bson:"progressivePrice"`
	IsPct                  string  `json:"isPct" bson:"isPct"`
	ProgressivePct         int64   `json:"progressivePct" bson:"progressivePct"`
	MaxPrice               float64 `json:"maxPrice" bson:"maxPrice"`
	Is24H                  string  `json:"is24H" bson:"is24H"`
	OvernightTime          string  `json:"overnightTime" bson:"overnightTime"`
	OvernightPrice         float64 `json:"overnightPrice" bson:"overnightPrice"`
	GracePeriod            int64   `json:"gracePeriod" bson:"gracePeriod"`
	TotalAmount            float64 `json:"totalAmount" bson:"totalAmount"`
	TotalProgressiveAmount float64 `json:"totalProgressiveAmount" bson:"totalProgressiveAmount"`
}

type TrxMember struct {
	DocNo              string  `json:"docNo" bson:"docNo"`
	PartnerCode        string  `json:"partnerCode" bson:"partnerCode"`
	FirstName          string  `json:"firstName" bson:"firstName"`
	LastName           string  `json:"lastName" bson:"lastName"`
	RoleType           string  `json:"roleType" bson:"roleType"`
	PhoneNumber        string  `json:"phoneNumber" bson:"phoneNumber"`
	Email              string  `json:"email" bson:"email"`
	Active             string  `json:"active" bson:"active"`
	ActiveAt           string  `json:"activeAt" bson:"activeAt"`
	NonActiveAt        *string `json:"nonActiveAt" bson:"nonActiveAt"`
	OuId               int64   `json:"ouId" bson:"ouId"`
	TypePartner        string  `json:"typePartner" bson:"typePartner"`
	CardNumber         string  `json:"cardNumber" bson:"cardNumber"`
	VehicleNumber      string  `json:"vehicleNumber" bson:"vehicleNumber"`
	RegisteredDatetime string  `json:"registeredDatetime" bson:"registeredDatetime"`
	DateFrom           string  `json:"dateFrom" bson:"dateFrom"`
	DateTo             string  `json:"dateTo" bson:"dateTo"`
	ProductId          int64   `json:"productId" bson:"productId"`
	ProductCode        string  `json:"productCode" bson:"productCode"`
}

type TrxOutstandingForClearSession struct {
	RefDocNo               string `json:"refDocNo" bson:"refDocNo"`
	TappingDate            string `json:"tappingDate" bson:"tappingDate"`
	TappingDatetime        string `json:"tappingDatetime" bson:"tappingDatetime"`
	CardNumberUuid         string `json:"cardNumberUuid" bson:"cardNumberUuid"`
	FlagClearSession       bool   `json:"flagClearSession" bson:"flagClearSession"`
	ClearDatetime          string `json:"clearDatetime" bson:"clearDatetime"`
	TrxOutstandingSnapshot Trx    `json:"trxOutstandingSnapshot" bson:"trxOutstandingSnapshot"`
}

type PolicyOuProductWithRules struct {
	OuId        int64   `json:"ouId" bson:"ouId,omitempty"`
	OuCode      string  `json:"ouCode" bson:"ouCode,omitempty"`
	OuName      string  `json:"ouName" bson:"ouName,omitempty"`
	ProductId   int64   `json:"productId" bson:"productId,omitempty"`
	ProductCode string  `json:"productCode" bson:"productCode,omitempty"`
	ProductName string  `json:"productName" bson:"productName,omitempty"`
	ServiceFee  float64 `json:"serviceFee" bson:"serviceFee,omitempty"`
	Price       float64 `json:"price" bson:"price,omitempty"`
}

type RequestPolicyOuProductWithRules struct {
	ProductId   int64   `json:"productId" bson:"productId"`
	ProductCode string  `json:"productCode" bson:"productCode"`
	ProductName string  `json:"productName" bson:"productName"`
	ServiceFee  float64 `json:"serviceFee" bson:"serviceFee"`
	Price       float64 `json:"price" bson:"price"`
}

type MemberLocal struct {
	Id                 int64   `json:"id"`
	MemberCode         string  `json:"member_code"`
	FirstName          string  `json:"first_name"`
	LastName           string  `json:"last_name"`
	RoleType           string  `json:"role_type"`
	PhoneNumber        string  `json:"phone_number"`
	Email              string  `json:"email"`
	Active             string  `json:"active"`
	ActiveAt           string  `json:"active_at"`
	NonActiveAt        *string `json:"non_active_at"`
	TypePartner        string  `json:"type_partner"`
	CardNumber         string  `json:"card_number"`
	RegisteredDatetime string  `json:"registered_datetime"`
	DateFrom           string  `json:"date_from"`
	DateTo             string  `json:"date_to"`
	CorporateId        int64   `json:"corporate_id"`
	CreatedAt          string  `json:"created_at"`
	CreatedBy          string  `json:"created_by"`
}

type TrxItemsCloud struct {
	DocNo            string                   `json:"docNo" bson:"docNo"`
	DocDate          string                   `json:"docDate" bson:"docDate"`
	DeviceId         string                   `json:"deviceId" bson:"deviceId"`
	Gate             string                   `json:"gate" bson:"gate"`
	CardNumberUUID   string                   `json:"cardNumberUuid" bson:"cardNumberUuid"`
	CardNumber       string                   `json:"cardNumber" bson:"cardNumber"`
	TypeCard         string                   `json:"typeCard" bson:"typeCard"`
	ExtLocalDatetime string                   `json:"extLocalDatetime" bson:"extLocalDatetime"`
	GrandTotal       float64                  `json:"grandTotal" bson:"grandTotal"`
	ServiceFee       float64                  `json:"serviceFee" bson:"serviceFee"`
	ProductId        int64                    `json:"productId" bson:"productId"`
	ProductCode      string                   `json:"productCode" bson:"productCode"`
	ProductName      string                   `json:"productName" bson:"productName"`
	PaymentMethod    string                   `json:"paymentMethod" bson:"paymentMethod"`
	Price            float64                  `json:"price" bson:"price"`
	ProductData      PolicyOuProductWithRules `json:"productData" bson:"productData"`
	MemberName       string                   `json:"memberName" bson:"memberName"`
	MemberCode       string                   `json:"memberCode" bson:"memberCode"`
	DataMember       *MemberLocal             `json:"dataMember" bson:"dataMember"`
	OuId             int64                    `json:"ouId" bson:"ouId"`
	OuName           string                   `json:"ouName" bson:"ouName"`
	OuCode           string                   `json:"ouCode" bson:"ouCode"`
	MDR              float64                  `json:"mdr" bson:"mdr"`
	MID              string                   `json:"mid" bson:"mid"`
	TID              string                   `json:"tid" bson:"tid"`
	CardType         string                   `json:"cardType" bson:"cardType"`
	CardPan          string                   `json:"cardPan" bson:"cardPan"`
	LastBalance      float64                  `json:"lastBalance" bson:"lastBalance"`
	ResponseWorker   string                   `json:"responseWorker" bson:"responseWorker"`
	CurrentBalance   float64                  `json:"currentBalance" bson:"currentBalance"`
	LogTrans         string                   `json:"logTrans" bson:"logTrans"`
	MerchantKey      string                   `json:"merchantKey" bson:"merchantKey"`
	SignatureKey     string                   `json:"signatureKey" bson:"signatureKey"`
	StatusTransaksi  string                   `json:"statusTransaksi" bson:"statusTransaksi"`
	QrText           string                   `json:"qrText" bson:"qrText"`
	FlagSyncData     bool                     `json:"flagSyncData" bson:"flagSyncData"`
}

type SyncTrx struct {
	TrxList []TrxItemsCloud `json:"trxList" bson:"trxList"`
}

type TrxItems struct {
	DocNo            string                   `json:"docNo" bson:"docNo"`
	DocDate          string                   `json:"docDate" bson:"docDate"`
	DeviceId         string                   `json:"deviceId" bson:"deviceId"`
	Gate             string                   `json:"gate" bson:"gate"`
	CardNumberUUID   string                   `json:"cardNumberUuid" bson:"cardNumberUuid"`
	CardNumber       string                   `json:"cardNumber" bson:"cardNumber"`
	TypeCard         string                   `json:"typeCard" bson:"typeCard"`
	ExtLocalDatetime string                   `json:"extLocalDatetime" bson:"extLocalDatetime"`
	GrandTotal       float64                  `json:"grandTotal" bson:"grandTotal"`
	ServiceFee       float64                  `json:"serviceFee" bson:"serviceFee"`
	ProductId        int64                    `json:"productId" bson:"productId"`
	ProductCode      string                   `json:"productCode" bson:"productCode"`
	ProductName      string                   `json:"productName" bson:"productName"`
	PaymentMethod    string                   `json:"paymentMethod" bson:"paymentMethod"`
	Price            float64                  `json:"price" bson:"price"`
	ProductData      PolicyOuProductWithRules `json:"productData" bson:"productData"`
	MemberName       string                   `json:"memberName" bson:"memberName"`
	MemberCode       string                   `json:"memberCode" bson:"memberCode"`
	DataMember       *MemberLocal             `json:"dataMember" bson:"dataMember"`
	OuId             int64                    `json:"ouId" bson:"ouId"`
	OuName           string                   `json:"ouName" bson:"ouName"`
	OuCode           string                   `json:"ouCode" bson:"ouCode"`
	MDR              float64                  `json:"mdr" bson:"mdr"`
	MID              string                   `json:"mid" bson:"mid"`
	TID              string                   `json:"tid" bson:"tid"`
	CardType         string                   `json:"cardType" bson:"cardType"`
	CardPan          string                   `json:"cardPan" bson:"cardPan"`
	LastBalance      float64                  `json:"lastBalance" bson:"lastBalance"`
	ResponseWorker   string                   `json:"responseWorker" bson:"responseWorker"`
	CurrentBalance   float64                  `json:"currentBalance" bson:"currentBalance"`
	LogTrans         string                   `json:"logTrans" bson:"logTrans"`
	MerchantKey      string                   `json:"merchantKey" bson:"merchantKey"`
	SignatureKey     string                   `json:"signatureKey" bson:"signatureKey"`
	StatusTransaksi  string                   `json:"statusTransaksi" bson:"statusTransaksi"`
	QrText           string                   `json:"qrText" bson:"qrText"`
	FlagSyncData     bool                     `json:"flagSyncData" bson:"flagSyncData"`
}

type TrxItemsMongo struct {
	BatchNo            string                   `json:"batchNo" bson:"batchNo"`
	DocNo              string                   `json:"docNo" bson:"docNo"`
	DocDate            string                   `json:"docDate" bson:"docDate"`
	RefBatchHeaderNo   string                   `json:"refBatchHeaderNo"`
	PaymentRefDocNo    string                   `json:"paymentRefDocno" bson:"paymentRefDocno"`
	MemberCode         string                   `json:"memberCode" bson:"memberCode"`
	MemberName         string                   `json:"memberName" bson:"memberName"`
	MemberType         string                   `json:"memberType" bson:"memberType"`
	CardNumberUuid     string                   `json:"cardNumberUuid" bson:"cardNumberUuid"`
	DataMember         *MemberLocal             `json:"dataMember" bson:"dataMember"`
	CardNumber         string                   `json:"cardNumber" bson:"cardNumber"`
	TypeCard           string                   `json:"typeCard" bson:"typeCard"`
	OuID               int64                    `json:"ouId" bson:"ouId"`
	OuName             string                   `json:"ouName" bson:"ouName"`
	OuCode             string                   `json:"ouCode" bson:"ouCode"`
	ProductID          int64                    `json:"productId" bson:"productId"`
	ProductCode        string                   `json:"productCode" bson:"productCode"`
	ProductName        string                   `json:"productName" bson:"productName"`
	ProductData        PolicyOuProductWithRules `json:"productData" bson:"productData"`
	Price              float64                  `jso0n:"price" bson:"price"`
	ServiceFee         float64                  `json:"serviceFee" bson:"serviceFee"`
	GrandTotal         float64                  `json:"grandTotal" bson:"grandTotal"`
	PaymentMethod      string                   `json:"paymentMethod" bson:"paymentMethod"`
	Mdr                *float64                 `json:"mdr" bson:"mdr"`
	Mid                *string                  `json:"mid" bson:"mid"`
	Tid                *string                  `json:"tid" bson:"tid"`
	CardType           *string                  `json:"cardType" bson:"cardType"`
	CardPan            *string                  `json:"cardPan" bson:"cardPan"`
	LastBalance        *float64                 `json:"lastBalance" bson:"lastBalance"`
	CurrentBalance     *float64                 `json:"currentBalance" bson:"currentBalance"`
	ResponseWorker     *string                  `json:"responseWorker" bson:"responseWorker"`
	ResponseTrxCode    string                   `json:"responseTrxCode" bson:"responseTrxCode"`
	StatusTrx          string                   `json:"statusTrx" bson:"statusTrx"`
	StatusDesc         string                   `json:"statusDesc" bson:"statusDesc"`
	ExtLocalDatetime   string                   `json:"extLocalDatetime" bson:"extLocalDatetime"`
	LogTrans           string                   `json:"logTrans" bson:"logTrans"`
	DeviceID           *string                  `json:"deviceId" bson:"deviceId"`
	MerchantKey        string                   `json:"merchantKey" bson:"merchantKey"`
	SignatureKey       string                   `json:"signatureKey" bson:"signatureKey"`
	SettlementDatetime *string                  `json:"settlementDatetime" bson:"settlementDatetime"`
	DeductDatetime     *string                  `json:"deductDatetime" bson:"deductDatetime"`
	Status             string                   `json:"status" bson:"status"`
	Msg                string                   `json:"msg" bson:"msg"`
	FlagErrorInternal  string                   `json:"flagErrorInternal" bson:"flagErrorInternal"`
	Gate               string                   `json:"gate" bson:"gate"`
	QRText             string                   `json:"qrText" bson:"qrText"`
	CallbackPayment    *map[string]interface{}  `json:"callbackPayment" bson:"callbackPayment"`
	UserId             *int64                   `json:"userId" bson:"userId"`
	Username           *string                  `json:"username" bson:"username"`
}

//-- WORKER APPS2PAY --
type RequestInquiryBatchApp2Pay struct {
	BatchCode string                   `json:"batchcode"`
	Detail    []RequestParkingApps2pay `json:"detail"`
}

type RequestParkingApps2pay struct {
	StatusCode          string  `json:"statusCode" bson:"statusCode"`
	Description         string  `json:"description" bson:"description"`
	MerchantNoRef       string  `json:"merchantNoRef" bson:"merchantNoRef"`
	TransactionLog      string  `json:"transactionLog" bson:"transactionLog"`
	DeviceID            string  `json:"deviceID" bson:"deviceID"`
	NoHeader            string  `json:"noheader" bson:"noheader"`
	BankNoRef           string  `json:"bankNoRef" bson:"bankNoRef"`
	CardType            string  `json:"cardType" bson:"cardType"`
	CardPan             string  `json:"cardPan" bson:"cardPan"`
	MID                 string  `json:"mid" bson:"mid"`
	TID                 string  `json:"tid" bson:"tid"`
	LastBalance         float64 `json:"lastBalance" bson:"lastBalance"`
	CurrentBalance      float64 `json:"currentBalance" bson:"currentBalance"`
	PaymentCategoryName string  `json:"paymentCategoryName" bson:"paymentCategoryName"`
	PaymentStatus       string  `json:"paymentStatus" bson:"paymentStatus"`
	TotalAmount         float64 `json:"totalAmount" bson:"totalAmount"`
	MDR                 float64 `json:"mdr" bson:"mdr"`
	Discount            float64 `json:"discount" bson:"discount"`
	PromoCode           string  `json:"promoCode" bson:"promoCode"`
	PromoIssuer         string  `json:"promoIssuer" bson:"promoIssuer"`
	CreatedAt           string  `json:"CreatedAt" bson:"CreatedAt"`
	SettleAt            string  `json:"settleAt" bson:"settleAt"`
	DeductAt            string  `json:"deductAt" bson:"deductAt"`
}

type RequestApps2PayCheckStatus struct {
	MerchantKey   string `json:"merchantKey"`
	MerchantNoRef string `json:"merchantNoRef"`
	WithBatchCode bool   `json:"withBatchCode"`
}
