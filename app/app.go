package app

import (
	"github.com/streadway/amqp"
	"rsyncconsumerdatapendingtoilet/repositories"
	"rsyncconsumerdatapendingtoilet/repositories/trxRepository"
	"rsyncconsumerdatapendingtoilet/services"

	"github.com/go-redis/redis"
)

func SetupApp(repo repositories.Repository, redis *redis.Client, rabbitMQ *amqp.Channel) services.UsecaseService {

	// Servicesc
	trxMongoRepo := trxRepository.NewTrxMongoRepository(repo)

	usecaseSvc := services.NewUsecaseService(redis, trxMongoRepo, rabbitMQ)

	return usecaseSvc
}
